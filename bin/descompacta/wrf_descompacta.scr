#!/bin/sh
#-------------------------------------------------------------------------------
#   Script per descompactar GRIB del WRF 
#-------------------------------------------------------------------------------
if [ $# -ne 3 ]; then
  echo $0 "error en Sintaxi: $0 CI XX aaaammddHH"
  echo -e "\t CI -> ECMWF/GFS"
  echo -e "\t XX -> 09,03,1p5"
  echo -e "\t HH -> 00 o 12"
  exit
fi
CI=$1
DX=$2           # Domini
NAME=$3         # data inici simulacio (aaaammddhh)

source /home/verifica/run/opt/machine.conf

DATA=${NAME:2:6} # data inici simulacio (aammdd)
ANY=${NAME:0:4}
MES=${NAME:4:2}
DIA=${NAME:6:2}
HORA=${NAME:8:2}  # hora inici simulacio (hh)

# Directoris
#-------------------------
case $CI in 
ECMWF) PREDIR="/data/meteo/wrf/run."$HORA"."$DX;	# Pronostic (format GRIB)
       LOCAL="/data/arxiu/verifica/wrf43-ecm/grib";
       WRKDIR=$DIADIR"/wrf-"$CI"/"$DX;
       iend=15;;
GFS)   PREDIR="/data/meteo/wrf/WRF35/GFS/run."$HORA"."$DX;
       LOCAL="/data/arxiu/verifica/wrf43-gfs/grib";
       WRKDIR=$DIADIR"/wrf-"$CI"/"$DX;
       iend=15;;
*)         escriu $0 "error" "CI no reconegudes, nomes ECMWF o GFS -> Sortim"; exit;;
esac
case $DX in
1p5) PREDIR="/data/meteo/wrf/run."$HORA"."$DX;        # Pronostic (format GRIB)
       LOCAL="/data/arxiu/verifica/wrf43-ecm/grib";
       WRKDIR=$DIADIR"/wrf-"$CI"/"$DX;
       iend=10;;
esac
NDX=$(echo $DX | sed 's/^0*//')
# neteja
rm -rf $WRKDIR
if ! (test -d $DIADIR); then mkdir -p $DIADIR; fi
if ! (test -d $WRKDIR); then mkdir -p $WRKDIR; fi
cd $WRKDIR
mkdir -p work_synop
mkdir -p work_raob
mkdir -p work_xema
mkdir -p work_boia
mkdir -p work_pcp
mkdir -p work_pcpobs
# Descompacta arxius grib 
#------------------------
 rm -f WRFPRS* wrf-prs* >& /dev/null
 if [ -s $PREDIR"/wrf-prs."$NAME"."$DX".tar.gz" ]; then
  escriu $0 "info" "Descompactant els GRIB del WRF+$CI"
  ln -sf $PREDIR"/wrf-prs."$NAME"."$DX".tar.gz"
  tar -xzf "wrf-prs."$NAME"."$DX".tar.gz"
 elif [ -s $LOCAL"/wrf-prs."$NAME"."$DX".tar.gz" ]; then # Busquem al directori local (execucio en diferit)
  escriu $0 "info" "Descompactant els GRIB del WRF+$CI (local)"
  ln -sf $LOCAL"/wrf-prs."$NAME"."$DX".tar.gz"
  tar -xzf "wrf-prs."$NAME"."$DX".tar.gz"
 else
  escriu $0 "error" "No hi ha sortides WRF disponibles"
  exit
 fi
for fitxer in `ls WRFPRS*` 
do
FF=`echo $fitxer |awk '{print substr($1,12,3)}'| awk '{ if (substr($0,1,1) == 0) {print substr($0,2,2)} else {print $0}}'`
$ECCBIN/grib_set -w shortName=mx2t -s table2Version=2,indicatorOfParameter=15 $fitxer grib1
$ECCBIN/grib_set -w shortName=mn2t -s table2Version=2,indicatorOfParameter=16 grib1 wrf-$CI-$DX"."$NAME"_"$FF.grib
rm grib1
done

# Prepara els camps per la comparació amb PCP_OBS i PCP_RADAR
if ( test "$DX" == "1p5" ) then
cp $OPTDIR/radar_3km.grid .
rgrid=radar_3km.grid
else
if ( test $DX -gt 4 ) then
cp $OPTDIR/radar_9km.grid .
rgrid=radar_9km.grid
else
cp $OPTDIR/radar_3km.grid .
rgrid=radar_3km.grid
fi
fi
#
for (( i = 1; i <= $iend; i++)); do
 case $i in
  1) H0="00"; H1="24";;
  2) H0="24"; H1="48";;
  3) H0="00"; H1="06";;
  4) H0="06"; H1="12";;
  5) H0="12"; H1="18";;
  6) H0="18"; H1="24";;
  7) H0="24"; H1="30";;
  8) H0="30"; H1="36";;
  9) H0="36"; H1="42";;
 10) H0="42"; H1="48";;
 11) H0="48"; H1="72";;
 12) H0="48"; H1="54";;
 13) H0="54"; H1="60";;
 14) H0="60"; H1="66";;
 15) H0="66"; H1="72";;
 esac
file_out=wrf-$CI-$DX"_"$NAME"_"$H0"_"$H1.grib
file_out_remap=wrf-$CI-$DX"_"$NAME"_"$H0"_"$H1"_remap.grib"
  if ( test $H0 -eq 0 ) then
$CDOBIN"/cdo" -s -selcode,61 WRFPRS_d01.024 aux.grib
$ECCBIN"/grib_set" -s stepRange=$H0-$H1,centre=7 aux.grib $file_out
$CDOBIN"/cdo" -s remapbil,$rgrid $file_out aux2.grib
$ECCBIN"/grib_set" -s stepRange=$H0-$H1,centre=7 aux2.grib $file_out_remap
  else
$CDOBIN"/cdo" -s -selcode,61 WRFPRS_d01.0$H0 aux0.grib
$CDOBIN"/cdo" -s -selcode,61 WRFPRS_d01.0$H1 aux1.grib
$CDOBIN"/cdo" -s sub aux1.grib aux0.grib aux.grib
$ECCBIN"/grib_set" -s stepRange=$H0-$H1,centre=7 aux.grib $file_out
$CDOBIN"/cdo" -s remapbil,$rgrid $file_out aux2.grib
$ECCBIN"/grib_set" -s stepRange=$H0-$H1,centre=7 aux2.grib $file_out_remap
  fi
done
rm aux*

escriu $0 "info" "FI de descompactacio WRF $DX $CI $NAME"
exit
