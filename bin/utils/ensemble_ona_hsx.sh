#!/bin/sh
source /home/verifica/run/opt/machine.conf
j=0
while [ $j -le 100 ];do
diabarra=`date +%Y/%m/%d --date="20220601 $j day"`
echo $diabarra
for FF in 24 48 72
do
case $FF in 
24) HORES="\"000000\",\"030000\",\"060000\",\"090000\",\"120000\",\"150000\",\"180000\",\"210000\",\"240000\"";;
48) HORES="\"270000\",\"300000\",\"330000\",\"360000\",\"390000\",\"420000\",\"450000\",\"480000\"";;
72) HORES="\"510000\",\"540000\",\"570000\",\"600000\",\"630000\",\"660000\",\"690000\",\"720000\"";;
esac

sed "s/NHORES/$HORES/g" $OPTDIR/STATAnalysisConfig_hsx > STATAnalysisConfig_hsx
cp /data/arxiu/verifica/ensembles_ona_ecmwf/$diabarra/point_stat_A03_ona_$FF"0000L"*_mpr.txt .
/opt/met-10/bin/stat_analysis -lookin /data/arxiu/verifica/ensembles_ona_ecmwf/$diabarra/*_mpr.txt -config STATAnalysisConfig_hsx -out fileout -v 0
awk '{print $3,$5,$4,$21}' fileout | grep "FCST " | awk '{print $2,$3,$4}'> aux1
awk '{print $3,$5,$4,$21}' fileout | grep "OBS " | awk '{print $2,$3,$4}'> aux2
join aux1 aux2 > amax

fileori=`ls point_stat_A03_ona_$FF"0000L"*_mpr.txt`
fileout=`echo $fileori | sed 's/mpr.txt/hsxpr.txt/g'`

head -1 $fileori > $fileout
for VAR in SWH_ENS_MIN SWH_ENS_MINUS SWH SWH_ENS_PLUS SWH_ENS_MAX
do
grep "$VAR " $fileori > entrada
grep "$VAR " amax > maxims
i=1
nlin=`wc -l maxims |awk '{print $1}'`
#
while [ $i -le $nlin ];do
id=`head -$i maxims |tail -1|awk '{print $2}'`
mfc=`head -$i maxims |tail -1|awk '{print $3}'`
mob=`head -$i maxims |tail -1|awk '{print $5}'`
replace_mpr entrada $id $mfc $mob >> $fileout
i=`expr $i + 1`
done
#
done
# ^end of vars
done
# ^end of FF
cp *_hsxpr.txt /data/arxiu/verifica/ensembles_ona_ecmwf/$diabarra
cp *_hsxpr.txt ../
((j++))
rm *txt
done 
