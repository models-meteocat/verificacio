set term post landscape color solid "Times_Roman,20"
set key outside below
set grid
set size 1,1
set datafile missing "NaN"
set xdata time
set timefmt '%Y%m%d%H'
set format x "%Y%m%d"
set xtics 604800
set xrange['2022060100':'2022090100']
set ylabel 'Alcada d ona significativa (m)'
set yrange[0:9]
set ytic 0.5
set style line 1 lt -1 lw 2
set style line 7 lt rgb "violet" lw 1 
set style line 2 lt 2 lc 1 lw 2
set style line 3 lt rgb "green" lw 1
set style line 4 lt 4 lw 1
set style line 5 lt 3 lw 1
set style line 6 lt 6 lw 1
set terminal png size 1700,500
set output "plot.png"
plot "valen.swan" using 1:($5) t "Buoy" with lines ls 1, "valen.ww3" using 1:($4) t "  WW3" with lines ls 3, "valen.swan" using 1:($4) t "  SWAN" with lines ls 5, "valen.ensifs" using 1:($4) t " ENS-ECWAM" with lines ls 7, 2.5 ls 6 notitle, 4 ls 2 notitle
quit
