#!/bin/sh
i=0
while [ $i -lt 100 ]; do
MYDA=`date +%y%m%d --date="20220601 $i day"`
MYANY=`date +%Y --date="20220601 $i day"`
MYMES=`date +%m --date="20220601 $i day"`
MYDIA=`date +%d --date="20220601 $i day"`
echo $MYDA
awk '{if (substr($4,0,2) < 24 ) {print substr($5,0,8)substr($5,10,2),$10,$27,$32,$33}}' /data/arxiu/verifica/onatge/swan/swan.00.03/$MYANY/$MYMES/$MYDIA/met-10/boia/point_stat_*_mpr.txt >> swan.dat
awk '{if (substr($4,0,2) < 24 ) {print substr($5,0,8)substr($5,10,2),$10,$27,$32,$33}}' /data/arxiu/verifica/onatge/ww3/ww3.00.03/$MYANY/$MYMES/$MYDIA/met-10/boia/point_stat_*_mpr.txt >> ww3.dat
#awk '{if (substr($4,0,2) < 24 ) {print substr($5,0,8)substr($5,10,2),$10,$27,$32,$33}}' /data/arxiu/verifica/onatge/wam/wam.00.12/$MYANY/$MYMES/$MYDIA/met-10/boia/point_stat_*$MYDA"_"??"0000V_mpr.txt" >> wam.dat
awk '{if ($4 < 240000 ) {print substr($5,0,8)substr($5,10,2),$10,$27,$32,$33,$28}}' /data/arxiu/verifica/ensembles_ona_ecmwf/$MYANY/$MYMES/$MYDIA/point_stat_*$MYDA"_"??"0000V_mpr.txt" >> ensifs.dat
#awk '{if (substr($4,0,2) < 24 ) {print substr($5,0,8)substr($5,10,2),$10,$27,$32,$33}}' /data/arxiu/verifica/wrf/wrf.00.03/$MYANY/$MYMES/$MYDIA/met-10/boia/point_stat_*$MYDA"_"??"0000V_mpr.txt" >> wrf.dat
i=`expr $i + 1`
done
