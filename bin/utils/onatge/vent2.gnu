set term post landscape color solid "Times_Roman,18"
set key outside below
set grid
set size 1,1
set datafile missing "NaN"
set xdata time
set timefmt '%Y%m%d%H'
set format x "%Y%m%d"
set xtics 604800
set xrange['2022060100':'2022090100']
set ylabel 'Velocitat del vent (m/s)'
set yrange [0:26]
set ytic 2
set style line 1 lt -1 lw 2
set style line 2 lt rgb "red" lw 1
set style line 3 lt 3 lw 1
set style line 4 lt 4 lw 1
set style line 5 lt 5 lw 1
set terminal png size 1700,500
set output "plot.png"
plot "valen.swan" using 1:(1.1252*$5) t "Buoy" with lines ls 1, "valen.swan" using 1:($4) t "  WRFv4.3" with lines ls 2
quit
