#!/bin/sh
i=0
for est in begur drago mahon tarra tbarc ttarr valen
do
#case $est in 
#begur) elat=" 41.9";;
#drago) elat=" 39.56";;
#mahon) elat=" 39.71";; 
#tarra) elat=" 40.69";; 
#tbarc) elat=" 41.32";; 
#ttarr) elat=" 41.07";; 
#valen) elat=" 39.51";;
#esac
for var in hs vent tm tp
do
case $var in
 hs) METV=HTSGW;METVW=SWH;;
 vent) METV=WIND;;
 tm) METV=PERPW;METVW=MWP;;
 tp) METV=PERSW;METVW=PP1D;;
esac
echo $est $METV
grep $METV swan.dat | grep $est > $est.swan
grep $METV ww3.dat | grep $est > $est.ww3
#grep "$METVW " ensifs.dat | grep "$elat" > $est.ensifs
grep "$METVW " ensifs.dat | grep $est > $est.ensifs
### Depuració de buits
i=0
LINE1=`head -1 $est.swan | awk '{print $2,$3,"NaN", "NaN"}'`
LINE2=`head -1 $est.ww3 | awk '{print $2,$3,"NaN", "NaN"}'`
#LINE2=`head -1 $est.ensifs | awk '{print $2,$3,"NaN", "NaN"}'`
while [ $i -lt 100 ]; do
MYDA=`date +%Y%m%d --date="20220601 $i day"`
echo $MYDA
j=0
while [ $j -lt 24 ];do
MYHH=`date +%H --date="20220601 $j hour"`
DATIN1=`grep $MYDA$MYHH $est.swan |wc -l`
DATIN2=`grep $MYDA$MYHH $est.ww3 |wc -l`
#DATIN3=`grep $MYDA$MYHH $est.wam |wc -l`
l=`expr $j - 1`
NEWDA=`date +%Y%m%d%H --date="$MYDA $l hour"`
if ( test $DATIN1 -ne 1 );then
sed "/$NEWDA/ a \ " $est.swan > test1
mv test1 $est.swan
fi
if ( test $DATIN2 -ne 1 );then
sed "/$NEWDA/ a \ " $est.ww3 > test1
mv test1 $est.ww3
fi
#if ( test $DATIN3 -ne 1 );then
#sed "/$NEWDA/ a \ " $est.wam > test1
#mv test1 $est.wam
#fi
j=`expr $j + 1`
done
i=`expr $i + 1`
done
### Fi de depuració de buits
#if (test $METV == "PERPW" ) then
awk '{ if ($5 < 25 ) {print $0} else {print $1,$2,$3,$4,"NaN"}}' $est.swan > ko 
mv ko $est.swan
awk '{ if ($5 < 25 ) {print $0} else {print $1,$2,$3,$4,"NaN"}}' $est.ww3 > ko
mv ko $est.ww3
awk '{ if ($5 < 25 ) {print $0} else {print $1,$2,$3,$4,"NaN"}}' $est.ensifs > ko
mv ko $est.ensifs
sed "s/MIBUOY/$est/g" $var.gnu > $var"2.gnu"
/opt/gnuplot-5.4.3/bin/gnuplot $var"2.gnu"
mv plot.png $var$est"est2022.png"
done
done
