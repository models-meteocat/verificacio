#!/bin/sh
i=0
while [ $i -lt 100 ]; do
MYDA=`date +%y%m%d --date="20220601 $i day"`
MYANY=`date +%Y --date="20220601 $i day"`
MYMES=`date +%m --date="20220601 $i day"`
MYDIA=`date +%d --date="20220601 $i day"`
echo $MYDA
for hh in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
do
u10=`grib_ls -w shortName=10u -l 41.32,2.21,1 /data/MODELS/WRF/V43_ECM/run.00.03/$MYANY/$MYMES/$MYDIA/wrf-03.20$MYDA"00_"$hh.grib | grep regular_ll | awk '{print $NF}'`
v10=`grib_ls -w shortName=10u -l 41.32,2.21,1 /data/MODELS/WRF/V43_ECM/run.00.03/$MYANY/$MYMES/$MYDIA/wrf-03.20$MYDA"00_"$hh.grib | grep regular_ll | awk '{print $NF}'`
echo 20$MYDA$hh > aux
awk -v u=$u10 -v v=$v10 '{print $0,"WIND","tbarc",sqrt(u*u+v*v),"999.9"}' aux >> tbarc.swan
u10=`grib_ls -w shortName=10u -l 41.07,1.2,1 /data/MODELS/WRF/V43_ECM/run.00.03/$MYANY/$MYMES/$MYDIA/wrf-03.20$MYDA"00_"$hh.grib | grep regular_ll | awk '{print $NF}'`
v10=`grib_ls -w shortName=10u -l 41.07,1.2,1 /data/MODELS/WRF/V43_ECM/run.00.03/$MYANY/$MYMES/$MYDIA/wrf-03.20$MYDA"00_"$hh.grib | grep regular_ll | awk '{print $NF}'`
echo 20$MYDA$hh > aux
awk -v u=$u10 -v v=$v10 '{print $0,"WIND","ttarr",sqrt(u*u+v*v),"999.9"}' aux >> ttarr.swan
done
i=`expr $i + 1`
done
