set term post landscape color solid "Times_Roman,18"
set key outside below
set grid
set size 1,1
set datafile missing "999.9"
set xdata time
set timefmt '%Y%m%d%H'
set format x "%Y%m%d"
set xtics 604800
set xrange['2017120100':'2018030100']
set ylabel 'Significant wave height (m)'
set yrange [0:8]
set ytic 1
set style line 1 lt -1 lw 2
set style line 2 lt 1 lw 2
set style line 3 lt 2 lw 2
set style line 4 lt 4 lw 2
set style line 5 lt 3 lw 2
set title 'Wave models 3 km vs MIBUOY (m) hivern 2017-2018' tc lt 3
set terminal png size 1500,500
set output "plot.png"
plot "MIBUOY.boi" using 2:9 t "Buoy" with lines ls 1, "MIBUOY.wam" using 1:4 t "WAM" with lines ls 2, "MIBUOY.ww3" using 1:4 t "WW3" with lines ls 3, "MIBUOY.swa" using 1:2 t "SWAN" with lines ls 5
quit
