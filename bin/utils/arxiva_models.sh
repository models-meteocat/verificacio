#!/bin/sh

# Carrega la llista de models que cal guardar en local per a la verificació
# al directori /data/arxiu/verifica
# Es tracta de versions de la sortida que només es guarden temporalment.
# N'especifica el nom, l'origen i el destí
# ----------------

unset MODEL_FILE
unset MODEL_ORIGEN
unset MODEL_DESTI
DATA8=`echo $DATA10 | awk '{print substr($0,0,8)}'`
DIRDAT=`date +%Y/%m/%d --date=$DATA8`
# BOLAM
MODEL_FILE[1]=bolam-grib2.$DATA10.08.tar.gz
MODEL_ORIGEN[1]="/data/meteo/isac"
MODEL_DESTI[1]="/data/arxiu/verifica/bolam/grib"

# MOLOCH
MODEL_FILE[2]=moloch-grib2.$DATA10.1p6.tar.gz
MODEL_ORIGEN[2]=/data/RAM_EXTRA/MODELS/MOLOCH-ECM/run.00.1p6/$DIRDAT
MODEL_DESTI[2]="/data/arxiu/verifica/moloch/grib"

# MOLOCH-ECM
MODEL_FILE[3]=moloch-grib2.$DATA10.1p6.tar.gz
MODEL_ORIGEN[3]=/data/RAM_EXTRA/MODELS/MOLOCH/run.00.1p6/$DIRDAT
MODEL_DESTI[3]="/data/arxiu/verifica/moloch-ecm/grib"

# UM
MODEL_FILE[4]=um-10.$DATA10.tar.gz
MODEL_ORIGEN[4]="/data/dades/UM"
MODEL_DESTI[4]="/data/arxiu/verifica/um/grib"

# WRF-ECM v43 3 km
MODEL_FILE[5]=wrf-prs.$DATA10.03.tar.gz
MODEL_ORIGEN[5]="/data/meteo/wrf/WRF43/ECM/run.00.03"
MODEL_DESTI[5]="/data/arxiu/verifica/wrf43-ecm/grib"

# WRF-ECM v43 1.5 km
MODEL_FILE[6]=wrf-prs.$DATA10.1p5.tar.gz
MODEL_ORIGEN[6]="/data/meteo/wrf/WRF43/ECM/run.00.1p5"
MODEL_DESTI[6]="/data/arxiu/verifica/wrf43-ecm/grib"

# WRF-GFS v43 9 km
MODEL_FILE[7]=wrf-prs.$DATA10.09.tar.gz
MODEL_ORIGEN[7]="/data/meteo/wrf/WRF43/GFS/run.00.09"
MODEL_DESTI[7]="/data/arxiu/verifica/wrf43-gfs/grib"

# WRF-GFS v43 3 km
MODEL_FILE[8]=wrf-prs.$DATA10.03.tar.gz
MODEL_ORIGEN[8]="/data/meteo/wrf/WRF43/GFS/run.00.03"
MODEL_DESTI[8]="/data/arxiu/verifica/wrf43-gfs/grib"
