#!/bin/sh
#-------------------------------------------------------------------------
# Script per verificar els models respecte XEMA, en base mensual/estacional/annual
#--------------------------------------------------------------------------------------------------
# Funcions
# ------------------------------------------------------------------------
source /home/verifica/run/opt/machine.conf
#----------------------------------------------------------
# Recollim parametres de l'script
# -------------------------------
if [ $# -ne 5 ]; then
  escriu $0 error "Sintaxi: $0 model XX HH XXXX cas"
  echo -e "\t model -> model (bolam,um, etc)"
  echo -e "\t XX -> resulucio (09,12,03,GLO)"
  echo -e "\t HH -> 00 o 12"
  echo -e "\t XXXX -> any per anys o estacions (ex. 2013)"
  echo -e "\t XXXX -> AAMM per mesos ex. (ex. 1306 juny del 2013)"
  echo -e "\t XXXX -> AAAAMMDD per dia"
  echo -e "\t cas -> any,hiv,pri,est,tar,dia"
  exit
fi
model=$1
XX=$2
HH=$3
DATA_INI=$4     # data inici simulacio (aaaammddhh)
cas=$5

# Directoris 
#--------------------------------------------------------------------------------------------------
if ( test "$cas" == "dia" );then
WRKDIR=$RESDIR"/"$model$XX"/dia/work_pcp_obs"
elif ( test "$cas" == "mes" );then
WRKDIR=$RESDIR"/"$model$XX"/mes/work_pcp_obs"
else
WRKDIR=$RESDIR"/"$model$XX"/work_pcp_obs"
fi

# Carreguem funcions locals:
cd $WRKDIR

# 1) WRF/COSMO/BOLAM: Verificacio acumulacions 24 h 
escriu $0 info "Calculant indexs verificacio $model $XX km XEMA"
  case $cas in
    dia) AA=`echo $DATA_INI|awk '{print substr($0,0,4)}'`;MM=`echo $DATA_INI|awk '{print substr($0,5,2)}'`;DD=`echo $DATA_INI|awk '{print substr($0,7,2)}'`; DAOUT=$AA/$MM/$DD"/met-10/pcp_obs";;
    mes) AA=`echo $DATA_INI|awk '{print substr($0,0,2)}'`;MM=`echo $DATA_INI|awk '{print substr($0,3,2)}'`;DAOUT="20"$AA/$MM;;
    *) DAOUT=$DATA_INI/resum;;
  esac
#
  case $model in
   bolam) OUTDIR=$OUTPATH"/bolam/bolam."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   wrf-ECMWF) OUTDIR=$OUTPATH"/wrf/wrf."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   wrf-GFS) OUTDIR=$OUTPATH"/wrf-gfs/wrf-gfs."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   um) OUTDIR=$OUTPATH"/um/um."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   moloch) OUTDIR=$OUTPATH"/moloch/moloch."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   moloch-ECMWF) OUTDIR=$OUTPATH"/moloch-ecm/moloch-ecm."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   arome) OUTDIR=$OUTPATH"/arome/arome."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   arpege) OUTDIR=$OUTPATH"/arpege/arpege."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   icon) OUTDIR=$OUTPATH"/icon/icon."$HH"."$XX"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   GFS) OUTDIR=$OUTPATH"/GFS/GFS."$HH"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   ECMWF) OUTDIR=$OUTPATH"/ECMWF/ECMWF."$HH"/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs;;
   nowcst-ctl) OUTDIR=$OUTPATH"/laps/ctl.xx.03/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs_now;;
   nowcst-3Dind) OUTDIR=$OUTPATH"/laps/3Dind.xx.03/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs_now;;
   nowcst-stmas) OUTDIR=$OUTPATH"/laps/stmas.xx.03/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs_now;;
   nowcst-3Dopt) OUTDIR=$OUTPATH"/laps/3Dopt.xx.03/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs_now;;
   nowcst-tl_ens_mean) OUTDIR=$OUTPATH"/laps/tl_ens_mean.xx.03/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs_now;;
   nowcst-tl_ens_mean9) OUTDIR=$OUTPATH"/laps/tl_ens_mean9.xx.03/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs_now;;
   nowcst-tl_ens_wmean) OUTDIR=$OUTPATH"/laps/tl_ens_wmean.xx.03/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs_now;;
   nowcst-tl_ens_wmean9) OUTDIR=$OUTPATH"/laps/tl_ens_wmean9.xx.03/"$DAOUT;FCONF=STATAnalysisConfig_pcp_obs_now;;
   *) echo "no reconec el model $model"; echo "Possibles (bolam,moloch,um,arome,wrf-ECMWF,wrf-GFS,wrf-ARSO,rf-WRF39GFS,GFS,ECMWF)";exit;;
  esac

# Iniciem
$DIRMET/stat_analysis -lookin ./*_mpr.txt -config $OPTDIR/$FCONF -out fileout -v 0 

if [ $FCONF == "STATAnalysisConfig_pcp_obs" ]; then
tail -n +2 myout_05_A24 >> myout_01_A24 
tail -n +2 myout_1_A24 >> myout_01_A24
tail -n +2 myout_2_A24 >> myout_01_A24
tail -n +2 myout_5_A24 >> myout_01_A24
tail -n +2 myout_10_A24 >> myout_01_A24
tail -n +2 myout_20_A24 >> myout_01_A24
tail -n +2 myout_50_A24 >> myout_01_A24
tail -n +2 myout_80_A24 >> myout_01_A24
tail -n +2 myout_100_A24 >> myout_01_A24
cp myout_01_A24 $OUTDIR/$model$XX"_PCP_OBS_A24_"$DATA_INI-$cas".cnt"
else
tail -n +2 myout_05_A01 >> myout_01_A01 
tail -n +2 myout_1_A01 >> myout_01_A01
tail -n +2 myout_2_A01 >> myout_01_A01
tail -n +2 myout_5_A01 >> myout_01_A01
tail -n +2 myout_10_A01 >> myout_01_A01
tail -n +2 myout_20_A01 >> myout_01_A01
tail -n +2 myout_50_A01 >> myout_01_A01
tail -n +2 myout_80_A01 >> myout_01_A01
tail -n +2 myout_100_A01 >> myout_01_A01
cp myout_01_A01 $OUTDIR/$model$XX"_PCP_OBS_A01_"$DATA_INI-$cas".cnt"
tail -n +2 myout_05_A06 >> myout_01_A06
tail -n +2 myout_1_A06 >> myout_01_A06
tail -n +2 myout_2_A06 >> myout_01_A06
tail -n +2 myout_5_A06 >> myout_01_A06
tail -n +2 myout_10_A06 >> myout_01_A06
tail -n +2 myout_20_A06 >> myout_01_A06
tail -n +2 myout_50_A06 >> myout_01_A06
tail -n +2 myout_80_A06 >> myout_01_A06
tail -n +2 myout_100_A06 >> myout_01_A06
cp myout_01_A06 $OUTDIR/$model$XX"_PCP_OBS_A06_"$DATA_INI-$cas".cnt"
for HIS in 00 03 06 09 12 15 18 21;do
cd $WRKDIR/$HIS
$DIRMET/stat_analysis -lookin ./*_mpr.txt -config $OPTDIR/$FCONF -out fileout -v 0
tail -n +2 myout_05_A01 >> myout_01_A01
tail -n +2 myout_1_A01 >> myout_01_A01
tail -n +2 myout_2_A01 >> myout_01_A01
tail -n +2 myout_5_A01 >> myout_01_A01
tail -n +2 myout_10_A01 >> myout_01_A01
tail -n +2 myout_20_A01 >> myout_01_A01
tail -n +2 myout_50_A01 >> myout_01_A01
tail -n +2 myout_80_A01 >> myout_01_A01
tail -n +2 myout_100_A01 >> myout_01_A01
cp myout_01_A01 $OUTDIR/$model$XX"_"$HIS"_PCP_OBS_A01_"$DATA_INI-$cas".cnt"
tail -n +2 myout_05_A06 >> myout_01_A06
tail -n +2 myout_1_A06 >> myout_01_A06
tail -n +2 myout_2_A06 >> myout_01_A06
tail -n +2 myout_5_A06 >> myout_01_A06
tail -n +2 myout_10_A06 >> myout_01_A06
tail -n +2 myout_20_A06 >> myout_01_A06
tail -n +2 myout_50_A06 >> myout_01_A06
tail -n +2 myout_80_A06 >> myout_01_A06
tail -n +2 myout_100_A06 >> myout_01_A06
cp myout_01_A06 $OUTDIR/$model$XX"_"$HIS"_PCP_OBS_A06_"$DATA_INI-$cas".cnt"
done
fi
exit
