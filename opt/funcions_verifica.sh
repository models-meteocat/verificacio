#!/bin/sh
#------------------------------------
# SCRIPT COMPARTIT DE FUNCIONS
#
# RAM - Juliol 2016
#------------------------------------

# 1. Filtra SYNOP presents a llista negra (BL)

function filtra_synop {
BL=$1;
SYNOP=$2;
num_bl=`cat $BL | wc -l | awk '{print $1}'` >& /dev/null;
num_synop=`cat $SYNOP | wc -l | awk '{print $1}'` >& /dev/null;
awk '
BEGIN {
# Llegim BL:
blacklist="'$BL'"
synop_file="'$SYNOP'"
 for(i=1;i<='$num_bl';i++){
 getline < blacklist
 BL[i]=substr($0,1,5)
 }
 for(i=1;i<='$num_synop';i++){
 getline < synop_file
 codi=substr($0,12,5)
 is_bl=0
  for(j=1;j<='$num_bl';j++){
  if (codi == BL[j]) { is_bl = 1 }
  }
  if ( is_bl == 0 ) { print $0 }
 }
}' >> metar.txt;
}

#-----------------------------------------------------------------
# 2. Recupera parella PRONOSTIC - METAR al fer verificacio mensual:

function recupera_metar {
  DATA10=$1;
  DATA_OBS=`/bin/date +%Y%m%d%H -d "${DATA10:2:6} ${DATA10:8:2} $PREV hours"`
  FILE_OBS="/home/verifica/obs/metar/"${DATA_OBS:0:4}"/"${DATA_OBS:4:2}"/"${DATA_OBS:6:2}"/metars_"${DATA_OBS:2:8}".nws.txt";      # fitxers METAR descodificats   
  if ! [ -s $FILE_OBS ]; then escriu "recupera_metar" "debug" "Falta METAR!"; return 1; fi
  ln -sf $FILE_OBS metar.txt	# entrada list_wrf2metar (obs METAR)
  case $model in
  wrf) 	
	$BINDIR/wrf_descompacta.scr $DX $DATA10 "ECMWF"
	FILE_PRE="WRFPRS_d01.0$PREV"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_metar" "debug" "Falta WRF!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
  	$BINDIR"/list_wrf2metar_nws" >& /dev/null
	rm "../work/WRF"* "../work/wrf"* "./info.txt"
  	mv output.txt $SRCDIR"/"$DD"/"$model"2metar."$DX"."$DATA10"_"$PREV".nws.txt";;
  wrf-gfs)	
	$BINDIR/wrf_descompacta.scr $DX $DATA10 "GFS"
	FILE_PRE="WRFPRS_d01.0$PREV"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_metar" "debug" "Falta WRF-GFS!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
  	$BINDIR"/list_wrf2metar_nws" >& /dev/null
	rm "../work/WRF"* "../work/wrf"* "./info.txt"
  	mv output.txt $SRCDIR"/"$DD"/"$model"2metar."$DX"."$DATA10"_"$PREV".nws.txt";;
  um)	
	$BINDIR/um_descompacta.scr $DX $DATA10
	FILE_PRE="um-"$DX"."$DATA10"_"$PREV".grib"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_metar" "debug" "Falta UM!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_um2metar_nws" > output.txt
	rm "../work/um"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2metar."$DX"."$DATA10"_"$PREV".nws.txt";;
  bolam)
	$BINDIR/bolam_descompacta.scr $DX $DATA10
	FILE_PRE="bolam-"$DX"."$DATA10"_"$PREV".grib2"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_metar" "debug" "Falta BOLAM!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_isac2metar_nws" > output.txt
	rm "../work/bolam"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2metar."$DX"."$DATA10"_"$PREV".nws.txt";;
  moloch)
	$BINDIR/moloch_descompacta.scr $DX $DATA10
	FILE_PRE="moloch-"$DX"."$DATA10"_"$PREV".grib2"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_metar" "debug" "Falta MOLOCH!"; return 1; fi;
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_isac2metar_nws" > output.txt
	rm "../work/moloch"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2metar."$DX"."$DATA10"_"$PREV".nws.txt";;
  esac;
  rm metar.txt grib;
}

#-----------------------------------------------------------------
# 3. Recupera parella PRONOSTIC - SYNOP al fer verificacio mensual:

function recupera_synop {
  DATA10=$1;
  DATA_OBS=`/bin/date +%Y%m%d%H -d "${DATA10:2:6} ${DATA10:8:2} $PREV hours"`
  FILE_OBS="/home/verifica/obs/synop/"${DATA_OBS:0:4}"/"${DATA_OBS:4:2}"/"${DATA_OBS:6:2}"/synop_"${DATA_OBS:2:8}".nws.txt";      # fitxers SYNOP descodificats   
  if ! [ -s $FILE_OBS ]; then escriu "recupera_synop" "debug" "Falta SYNOP!"; return 1; fi
  ln -sf $FILE_OBS metar.txt	# entrada list_wrf2metar (obs SYNOP)
  case $model in
  wrf) 	
	$BINDIR/wrf_descompacta.scr $DX $DATA10 "ECMWF"
	FILE_PRE="WRFPRS_d01.0$PREV"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_synop" "debug" "Falta WRF!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
  	$BINDIR"/list_wrf2metar_nws" >& /dev/null
	rm "../work/WRF"* "../work/wrf"* "./info.txt"
  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  wrf-gfs)	
	$BINDIR/wrf_descompacta.scr $DX $DATA10 "GFS"
	FILE_PRE="WRFPRS_d01.0$PREV"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_synop" "debug" "Falta WRF-GFS!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
  	$BINDIR"/list_wrf2metar_nws" >& /dev/null
	rm "../work/WRF"* "../work/wrf"* "./info.txt"
  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  um)	
	$BINDIR/um_descompacta.scr $DX $DATA10
	FILE_PRE="um-"$DX"."$DATA10"_"$PREV".grib"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_synop" "debug" "Falta UM!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_um2metar_nws" > output.txt
	rm "../work/um"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  bolam)
	$BINDIR/bolam_descompacta.scr $DX $DATA10
	FILE_PRE="bolam-"$DX"."$DATA10"_"$PREV".grib2"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_synop" "debug" "Falta BOLAM!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_isac2metar_nws" > output.txt
	rm "../work/bolam"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  moloch)
	$BINDIR/moloch_descompacta.scr $DX $DATA10
	FILE_PRE="moloch-"$DX"."$DATA10"_"$PREV".grib2"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_synop" "debug" "Falta MOLOCH!"; return 1; fi;
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_isac2metar_nws" > output.txt
	rm "../work/moloch"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  esac;
  rm metar.txt grib;
}

#------------------------------------------------------------------
# 4. Recupera parelles PRONOSTIC - RAOB al fer verificacio mensual:

function recupera_raob {
  DATA10=$1;
  DATA_OBS=`/bin/date +%Y%m%d%H -d "${DATA10:2:6} ${DATA10:8:2} $PREV hours"`
  FILE_OBS="/home/verifica/obs/raob/"${DATA_OBS:0:4}"/"${DATA_OBS:4:2}"/"${DATA_OBS:6:2}"/raobs_"${DATA_OBS:2:8}".nws.txt";      # fitxers RAOB descodificats  
  if ! [ -s $FILE_OBS ]; then escriu "recupera_raob" "debug" "Falta RAOB!"; return 1; fi
  rm -rf RAOB >& /dev/null
  ln -sf $FILE_OBS RAOB	# entrada list_wrf2raob.scr (obs RAOB)
  case $model in
  wrf) 	
	$BINDIR/wrf_descompacta.scr $DX $DATA10 "ECMWF"
	FILE_PRE="WRFPRS_d01.0$PREV"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_raob" "debug" "Falta WRF!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
  	$BINDIR"/list_wrf2raob.scr" >& /dev/null
	rm "../work/WRF"* "../work/wrf"* "./info.txt";;
#  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  wrf-gfs)	
	$BINDIR/wrf_descompacta.scr $DX $DATA10 "GFS"
	FILE_PRE="WRFPRS_d01.0$PREV"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_raob" "debug" "Falta WRF-GFS!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
  	$BINDIR"/list_wrf2raob.scr" >& /dev/null
	rm "../work/WRF"* "../work/wrf"* "./info.txt";;
#  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  um)	
	$BINDIR/um_descompacta.scr $DX $DATA10
	FILE_PRE="um-"$DX"."$DATA10"_"$PREV".grib"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_raob" "debug" "Falta UM!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_um2raob.scr" >& /dev/null
	rm "../work/um"*;;
#  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  bolam)
	$BINDIR/bolam_descompacta.scr $DX $DATA10
	FILE_PRE="bolam-"$DX"."$DATA10"_"$PREV".grib2"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_raob" "debug" "Falta BOLAM!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_isac2raob.scr" >& /dev/null
	rm "../work/bolam"*;;
#  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  moloch)
	$BINDIR/moloch_descompacta.scr $DX $DATA10
	FILE_PRE="moloch-"$DX"."$DATA10"_"$PREV".grib2"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_raob" "debug" "Falta MOLOCH!"; return 1; fi;
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_isac2raob.scr" >& /dev/null
	rm "../work/moloch"*;;
#  	mv output.txt $SRCDIR"/"$DD"/"$model"2synop."$DX"."$DATA10"_"$PREV".nws.txt";;
  esac;
  rm -f RAOB grib;

  for level in 925 850 700 500 300; do	# renombra fitxers ???_valida.txt que generen els list_MOD2raob.scr
      FILE_INP=$level"_valida.txt"                                        # fitxers entrada (sortides list_isac2raob.scr)
      FILE_OUT=$SRCDIR"/"$DD"/"$model"2raob"$level"."$DX"."$DATA10"_"$PREV".nws.txt"       # fitxers finals
      if [ -s $FILE_INP ]; then
        escriu $0 "info" " -> $FILE_OUT"
        mv $FILE_INP $FILE_OUT
      fi
  done;
 
}
#------------------------------------------------------------------
# 5. Recupera parella PRONOSTIC - XEMA al fer verificacio mensual:

function recupera_xema {
  DATA10=$1;
  DATA_OBS=`/bin/date +%Y%m%d%H -d "${DATA10:2:6} ${DATA10:8:2} $PREV hours"`
  FILE_OBS="/home/verifica/obs/xema/"${DATA_OBS:0:4}"/"${DATA_OBS:4:2}"/"${DATA_OBS:6:2}"/xema_"${DATA_OBS:2:8}".txt";      # fitxers XEMA   
  if ! [ -s $FILE_OBS ]; then escriu "recupera_xema" "debug" "Falta XEMA!"; return 1; fi
  #sqlplus -S emac/emac@BDEMEXP @"/home/verifica/bin/verifica_XEMA_list.sql" XEMA 20$DATA_OBS
  #sqlplus -S emac/emac@BDEMPRO @"/home/verifica/bin/verifica_XEMA_list.sql" XEMA 20$DATA_OBS
  sqlplus -S emac/emac@SMCORA @"/home/verifica/bin/verifica_XEMA_list.sql" XEMA 20$DATA_OBS
  for CODI in `cat XEMA.lst | awk -F ";" '{print $1";"}'`; do
  grep -h $CODI $FILE_OBS >> xema.lst 2> /dev/null # entrada list_wrf2xema (obs XEMA)
  done
  case $model in
  wrf) 	
	$BINDIR/wrf_descompacta.scr $DX $DATA10 "ECMWF"
	FILE_PRE="WRFPRS_d01.0$PREV"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_xema" "debug" "Falta WRF!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
  	$BINDIR"/list_wrf2xema" >& /dev/null
	rm "../work/WRF"* "../work/wrf"* "./info.txt"
  	mv output.txt $SRCDIR"/"$DD"/"$model"2xema."$DX"."$DATA10"_"$PREV".txt";;
  wrf-gfs)	
	$BINDIR/wrf_descompacta.scr $DX $DATA10 "GFS"
	FILE_PRE="WRFPRS_d01.0$PREV"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_xema" "debug" "Falta WRF-GFS!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
  	$BINDIR"/list_wrf2xema" >& /dev/null
	rm "../work/WRF"* "../work/wrf"* "./info.txt"
  	mv output.txt $SRCDIR"/"$DD"/"$model"2xema."$DX"."$DATA10"_"$PREV".txt";;
  um)	
	$BINDIR/um_descompacta.scr $DX $DATA10
	FILE_PRE="um-"$DX"."$DATA10"_"$PREV".grib"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_xema" "debug" "Falta UM!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_um2xema" > output.txt
	rm "../work/um"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2xema."$DX"."$DATA10"_"$PREV".txt";;
  bolam)
	$BINDIR/bolam_descompacta.scr $DX $DATA10
	FILE_PRE="bolam-"$DX"."$DATA10"_"$PREV".grib2"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_xema" "debug" "Falta BOLAM!"; return 1; fi
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_isac2xema" > output.txt
	rm "../work/bolam"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2xema."$DX"."$DATA10"_"$PREV".txt";;
  moloch)
	$BINDIR/moloch_descompacta.scr $DX $DATA10
	FILE_PRE="moloch-"$DX"."$DATA10"_"$PREV".grib2"
	if ! [ -s "../work/"$FILE_PRE ]; then escriu "recupera_xema" "debug" "Falta MOLOCH!"; return 1; fi;
	ln -sf "../work/"$FILE_PRE grib
	$BINDIR"/list_isac2xema" > output.txt
	rm "../work/moloch"*
  	mv output.txt $SRCDIR"/"$DD"/"$model"2xema."$DX"."$DATA10"_"$PREV".txt";;
  GFS)
	$BINDIR/GLO_descompacta.scr GFS $DATA10
	FILE_PRE="GFS-0p25_"${DATA10:8:2}"_"$PREV"_"$DATA10"_eu"
	if ! [ -s "../work_global/"$FILE_PRE ]; then escriu "recupera_xema" "debug" "Falta GFS!"; return 1; fi
	ln -sf "../work_global/"$FILE_PRE grib
   	$BINDIR"/list_gfs2xema" > output.txt 2>/dev/null
  	mv output.txt $SRCDIR"/"$DD"/"$model"2xema."$DATA10"_"$PREV".txt";;
  esac;
  rm xema.lst grib;
}


function xema2MET {
arx=$1;
lines=`cat $arx | wc -l` >& /dev/null;
awk 'BEGIN {
FS=";"
file1="'$arx'"

 for(i=1;i<='$lines';i++){
 getline < file1

  aammddhh=$1
  id=$2
  lon=$4
  lat=$5
  z=$6
  t=$7
  rh=$8
  ws=$9
  wd=$10
  deg=57.2957795131
  us=ws*sin(wd/deg)
  vs=ws*cos(wd/deg)
  pcp01=$13
  pcp06=$15
  pcp24=$16
  tx=$17
  tn=$18
  pr=$19*100
  rs=$20
  ratxa=$24
  ratxa3h=$25
  if ( t > -70.0 ) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 11 0 2 NA %6.2f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,t+273.15) }
  if ( rh > -1.9) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 52 0 2 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,rh) }
  if ( ws > -1.9 ) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 32 0 10 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,ws) }
  if ( wd > -1.9) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 31 0 10 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,wd) 
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 33 0 10 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,us) 
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 34 0 10 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,vs) }
    if ( pcp01 > -1.9) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 61 1 0 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pcp01) }
    if ( pcp06 > -1.9) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 61 6 0 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pcp06) }
  if ( pcp24 > -1.9) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 61 24 0 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pcp24) }
  if ( tx > -70.9) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 15 24 2 NA %6.2f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,tx+273.15) }
  if ( tn > -70.9) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 16 24 2 NA %6.2f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,tn+273.15) }
  if ( pr > -1.9 ) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 1 0 2 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pr) }
  if ( rs > -1.9) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 204 0 0 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,rs) }
  if ( ratxa > 0.1) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 180 0 10 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,ratxa) }
  if ( ratxa3h > 0.1) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 180 3 10 NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,ratxa3h) }
 }
 }' >> xema.ascii;
}

function replace_mpr {
arxiumet=$1;
lines=`cat $arxiumet | wc -l` >& /dev/null;
SID=$2
FCST=$3;
OBS=$4;
awk 'BEGIN {
file1="'$arxiumet'"
FCT="'$FCST'"
OB="'$OBS'"
ID="'$SID'"
 for(i=1;i<='$lines';i++){
getline < file1
if ( ID == $27 ) {
print($1,$2,$3,$4,$5,$6,"240000",$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,FCT,OB,$34,$35,$36,$37)}
}}'
}

function synop2MET {
arx=$1;
lines=`cat $arx | wc -l` >& /dev/null;
awk 'BEGIN {
file1="'$arx'"

 for(i=1;i<='$lines';i++){
 getline < file1

  aammddhhmm=$1
  id=$2
  lon=substr($0,40,8) + 0.
  lat=substr($0,50,8) + 0.
  z=-9999
  pr=substr($0,59,8) + 0.
  pr=pr*100
  t=substr($0,67,8) + 0.
  td=substr($0,76,8) + 0.
  rh=substr($0,86,8) + 0.
  wd=substr($0,95,8) + 0.
deg=57.2957795131
 ws=substr($0,104,8) + 0.
  us=ws*sin(wd/deg)
  vs=ws*cos(wd/deg)
  if ( lat > 32.0 && lat < 45. && lon > -11.0 && lon < 10.0 ) {
  if ( t > -90.0) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 11 0 2 NA %6.2f\n",id,substr(aammddhhmm,1,6),substr(aammddhhmm,7,2),lat,lon,z,t+273.15) }
  if ( rh > -90.0) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 52 0 2 NA %6.1f\n",id,substr(aammddhhmm,1,6),substr(aammddhhmm,7,2),lat,lon,z,rh) }
  if ( ws > -90.0) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 32 0 10 NA %6.1f\n",id,substr(aammddhhmm,1,6),substr(aammddhhmm,7,2),lat,lon,z,ws) }
  if ( wd > -90.0) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 31 0 10 NA %6.1f\n",id,substr(aammddhhmm,1,6),substr(aammddhhmm,7,2),lat,lon,z,wd)
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 33 0 10 NA %6.1f\n",id,substr(aammddhhmm,1,6),substr(aammddhhmm,7,2),lat,lon,z,us) 
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 34 0 10 NA %6.1f\n",id,substr(aammddhhmm,1,6),substr(aammddhhmm,7,2),lat,lon,z,vs) }
  if ( td > -90.0) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 17 0 2 NA %6.1f\n",id,substr(aammddhhmm,1,6),substr(aammddhhmm,7,2),lat,lon,z,td+273.15) }
  if ( pr > -90.0) {
  printf("ADPSFC %s 20%s_%s0000 %6.3f %6.3f %4i 2 0 0 NA %6.1f\n",id,substr(aammddhhmm,1,6),substr(aammddhhmm,7,2),lat,lon,z,pr) }
 }}
 }' >> synop.ascii;
}

function raob2MET {
total=`cat RAOB | wc -l`;
lines=`cat lines | wc -l`;
awk 'BEGIN{
file1="RAOB"
split("'`awk 'BEGIN { ORS=";"}; { print $1 }' lines`'", lin, ";")
 k=1
 ns=0
 ne=0
 for(i=1;i<='$total';i++){
 getline < file1

if ( i == lin[k] ) {
 k=k+1

  aammddhh=substr($0,37,8)
  id=substr($0,1,5)
  lon=substr($0,69,7)
  lat=substr($0,61,7)
  z=substr($0,55,4)
  ns=i+5
  ne=substr($0,48,3) + ns
}
if ( i >= ns ) {
if ( i <= ne ) {
  pr=substr($0,5,7) + 0.
  gh=substr($0,14,7) + 0.
  t=substr($0,23,6) + 0.
  td=substr($0,31,6) + 0.
  wd=substr($0,39,6) + 0.
 ws=substr($0,47,6) * 0.514444
 deg=57.2957795131
  us=ws*sin(wd/deg)
  vs=ws*cos(wd/deg)
  
  if (gh > -900.0) {
  printf("ADPUPA %s 20%s_%s0000   %6.2f %6.2f %4i 7 %7.2f %5i NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pr,gh,gh) }
  if (t > -900.0) {
  printf("ADPUPA %s 20%s_%s0000   %6.2f %6.2f %4i 11 %7.2f %5i NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pr,gh,t+273.15) }
  if (ws > -90.0) {
  printf("ADPUPA %s 20%s_%s0000   %6.2f %6.2f %4i 32 %7.2f %5i NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pr,gh,ws) }
  if (wd > -900.0) {
  printf("ADPUPA %s 20%s_%s0000   %6.2f %6.2f %4i 31 %7.2f %5i NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pr,gh,wd)
  printf("ADPUPA %s 20%s_%s0000   %6.2f %6.2f %4i 33 %7.2f %5i NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pr,gh,us)
  printf("ADPUPA %s 20%s_%s0000   %6.2f %6.2f %4i 34 %7.2f %5i NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pr,gh,vs) }
  if (td > -900.0) {
  printf("ADPUPA %s 20%s_%s0000   %6.2f %6.2f %4i 17 %7.2f %5i NA %6.1f\n",id,substr(aammddhh,1,6),substr(aammddhh,7,2),lat,lon,z,pr,gh,td+273.15) }
}}
}}' > raob.ascii ;

}

function boia2MET {
arx=$1;
lines=`cat $arx | wc -l` >& /dev/null;
awk 'BEGIN {
file1="'$arx'"

 for(i=1;i<='$lines';i++){
 getline < file1

  aaaammddhh=$2
  id=$1
  lon=$4
  lat=$3
  z=0
  if ( id == "ttarr" ) { lon=1.2 }
  t=$6
  ws3m=$7
  wd=$8
  deg=57.2957795131
  nws=(0.37-0.0881*log(ws3m))/1.0607
  ws=ws3m*((10/3)^nws)

  us=ws*sin(wd/deg)
  vs=ws*cos(wd/deg)

  wahs=$9
  watm=$10
  watp=$11
  wadp=$12
  cursp=$13
  curdi=$14
  seate=$15
  if ( aaaammddhh > 1000 && t > -1.9 && t < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 11 0 2 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,2,t+273.15) }
  if ( aaaammddhh > 1000 && ws > 0.0 && ws < 699. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 32 0 10 NA %6.1f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,10,ws) }
  if ( aaaammddhh > 1000 && wd > -1.9 && wd < 399. && ws > 0.0 ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 31 0 10 NA %6.1f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,10,wd) 
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 33 0 10 NA %6.1f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,10,us) 
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 34 0 10 NA %6.1f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,10,vs) }
  if ( aaaammddhh > 1000 && wadp > -1.9 && wadp < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 107 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,wadp) }
 if ( aaaammddhh > 1000 && wahs > -1.9 && wahs < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 100 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,wahs) }
 if ( aaaammddhh > 1000 && watm > -1.9 && watm < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 108 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,watm) }
 if ( aaaammddhh > 1000 && watp > -1.9 && watp < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 110 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,watp) }
 if ( aaaammddhh > 1000 && cursp > -1.9 && cursp < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 48 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,cursp) }
 if ( aaaammddhh > 1000 && curdi > -1.9 && curdi < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 47 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,curdi) }
 if ( aaaammddhh > 1000 && seate > -1.9 && seate < 100. ) {
 printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 80 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,0,t+273.15) }
}
 }' >> boia.ascii;
}

function pde2MET {
arx=$1;
idt=`echo ${arx:6:5}`
lines=`cat $arx | wc -l` >& /dev/null;

awk 'BEGIN {
file1="'$arx'"
id="'$idt'"

 for(i=1;i<='$lines';i++){
 getline < file1

  aaaammddhh=$1
  if ( id == "mahon" ) { lon=4.42 ; lat=39.71 }
  if ( id == "begur" ) { lat= 41.90 ; lon = 3.64}
  if ( id == "drago" ) { lat= 39.56 ; lon =  2.10}
  if ( id == "valen" ) { lon=-0.31 ; lat=39.45 }
  if ( id == "tarra" ) { lon=1.47 ; lat=40.69 }
  if ( id == "ttarr" ) { lon= 1.19; lat= 41.07}
  if ( id == "tbarc" ) { lon= 2.21; lat= 41.32}

  z=0
  wahs=$3
  watm=$5
  watp=$4

  if ( wadp > 0 && wadp < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 107 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,wadp) }
 if ( wahs > 0 && wahs < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 100 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,wahs) }
 if ( watm > 0 && watm < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 108 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,watm) }
 if ( watp > 0 && watp < 999. ) {
  printf("SFCSHP %s %s_%s0000 %6.3f %6.3f %4i 110 0 0 NA %6.2f\n",id,substr(aaaammddhh,1,8),substr(aaaammddhh,9,2),lat,lon,z,watp) }
 }
 }' >> pde.ascii;
}
