////////////////////////////////////////////////////////////////////////////////
//
// Ensemble-Stat configuration file.
//
// For additional information, see the MET_BASE/config/README file.
//
////////////////////////////////////////////////////////////////////////////////

//
// Output model name to be written
//
model = "PME";

//
// Output description to be written
// May be set separately in each "obs.field" entry
//
desc = "NA";

//
// Output observation type to be written
//
obtype = "REA";

////////////////////////////////////////////////////////////////////////////////

//
// Verification grid
// May be set separately in each "field" entry
//
regrid = {
   to_grid    = FCST;
   method     = NEAREST;
   width      = 1;
   vld_thresh = 0.75;
   shape      = SQUARE;
}

////////////////////////////////////////////////////////////////////////////////

//
// May be set separately in each "field" entry
//
cat_thresh = [ <=258.15, <=263.15, <=268.15, <=273.15, >=293.15, >=298.15, >=303.15, >=308.15, >=313.15 ];
nc_var_str    = "";

//
// Ensemble product fields to be processed
//
ens = {
   ens_thresh = 0.3;
   vld_thresh = 0.3;

   field = [
      {
         name       = "TMP";
         level      = "Z0";
      }
   ];
}

////////////////////////////////////////////////////////////////////////////////

//
// Neighborhood ensemble probabilities
//
nbrhd_prob = {
   width      = [ 9 ];
   shape      = CIRCLE;
   vld_thresh = 0.0;
}

//
// NMEP smoothing methods
//
//nmep_smooth = {
//   vld_thresh      = 0.0;
//   shape           = CIRCLE;
//   gaussian_dx     = 81.27;
//   gaussian_radius = 120;
//   type = [
//      {
//         method = GAUSSIAN;
//         width  = 1;
//      }
//   ];
//}

////////////////////////////////////////////////////////////////////////////////

//
// Forecast and observation fields to be verified
//
fcst = {
   field = [
      {
         name  = "TMP";
         level      = "Z0";
         cat_thresh = [ <=258.15, <=263.15, <=268.15, <=273.15, >=293.15, >=298.15, >=303.15, >=308.15, >=313.15 ];
         prob_cat_thresh = [>0.3, >0.4, >0.5, >0.6, >0.7];
      }
   ];
}
obs = fcst;


////////////////////////////////////////////////////////////////////////////////

//
// Point observation filtering options
// May be set separately in each "obs.field" entry
//
message_type   = [ "ADPSFC" ];
sid_inc        = [];
sid_exc        = [];
obs_thresh     = [ NA ];
obs_quality_inc    = [];
duplicate_flag = NONE;
obs_summary    = NONE;
obs_perc_value = 50;
skip_const     = TRUE;

//
// Observation error options
// Set dist_type to NONE to use the observation error table instead
// May be set separately in each "obs.field" entry
//
obs_error = {
   flag             = FALSE;   // TRUE or FALSE
   dist_type        = LOGNORMAL ;    // Distribution type
   dist_parm        = [];      // Distribution parameters
   inst_bias_scale  = 1.0;     // Instrument bias scale adjustment
   inst_bias_offset = 0.0;     // Instrument bias offset adjustment
   min              = NA;      // Valid range of data
   max              = NA;
}

//
// Mapping of message type group name to comma-separated list of values
//
message_type_group_map = [
   { key = "SURFACE"; val = "ADPSFC,SFCSHP,MSONET";               },
   { key = "ANYAIR";  val = "AIRCAR,AIRCFT";                      },
   { key = "ANYSFC";  val = "ADPSFC,SFCSHP,ADPUPA,PROFLR,MSONET"; },
   { key = "ONLYSF";  val = "ADPSFC,SFCSHP";                      }
];

//
// Ensemble bin sizes
// May be set separately in each "obs.field" entry
//
ens_ssvar_bin_size = 1.0;
ens_phist_bin_size = 0.05;

//
// Categorical thresholds to define ensemble probabilities
// May be set separately in each "fcst.field" entry
//
//prob_cat_thresh = [ >0.4 ; >5.0 ];

////////////////////////////////////////////////////////////////////////////////

//
// Climatology data
//
climo_mean = {

   file_name = [];
   field     = [];

   regrid = {
      method     = NEAREST;
      width      = 1;
      vld_thresh = 0.3;
      shape      = SQUARE;
   }

   time_interp_method = DW_MEAN;
   day_interval       = 31;
   hour_interval      = 6;
}

climo_stdev = climo_mean;
climo_stdev = {
   file_name = [];
}

//
// May be set separately in each "obs.field" entry
//
climo_cdf = {
   cdf_bins    = 1;
   center_bins = FALSE;
   write_bins  = TRUE;
}

////////////////////////////////////////////////////////////////////////////////

//
// Point observation time window
//
obs_window = {
   beg = -1800;
   end =  1800;
}

////////////////////////////////////////////////////////////////////////////////

//
// Verification masking regions
//
mask = {
   grid  = [ "FULL" ];
   poly  = [];
   sid   = [];
   llpnt = [];
}

////////////////////////////////////////////////////////////////////////////////

//
// Confidence interval settings
//
ci_alpha  = [ 0.05 ];

////////////////////////////////////////////////////////////////////////////////

//
// Interpolation methods
//
interp = {
vld_thresh = 0.75;

type = [
{
method = GEOG_MATCH;
width = 1;
}
];
}

land_mask = {
flag = TRUE;
file_name = ["/home/verifica/run/opt/mask1km.grb2"];
field = { name = "LAND"; level = "L0"; }
regrid = { method = NEAREST ; width = 1; }
thresh = >0.5;
}

topo_mask = {
   flag       = FALSE;
   file_name  = ["/home/verifica/run/opt/mde1km.grb"];
   field      = { name = "DIST"; level = "L0"; }
   regrid     = { method = NEAREST ; width = 1; }
   use_obs_thresh =  ge-300&&le300;
   interp_fcst_thresh = ge-300&&le300;
}

////////////////////////////////////////////////////////////////////////////////

//
// Statistical output types
//
output_flag = {
   ecnt  = BOTH;
   rps   = BOTH;
   rhist = BOTH;
   phist = BOTH;
   orank = BOTH;
   ssvar = BOTH;
   relp  = BOTH;
}

////////////////////////////////////////////////////////////////////////////////

//
// Ensemble product output types
//
ensemble_flag = {
   latlon    = TRUE;
   mean      = TRUE;
   stdev     = TRUE;
   minus     = TRUE;
   plus      = TRUE;
   min       = TRUE;
   max       = TRUE;
   range     = TRUE;
   vld_count = TRUE;
   frequency = TRUE;
   nep       = TRUE;
   nmep      = FALSE;
   rank      = TRUE;
   weight    = FALSE;
}

////////////////////////////////////////////////////////////////////////////////

//
// Random number generator
//
rng = {
   type = "mt19937";
   seed = "";
}

////////////////////////////////////////////////////////////////////////////////

grid_weight_flag = NONE;
output_prefix    = "TG03";
version          = "V10.0.0";

////////////////////////////////////////////////////////////////////////////////
