////////////////////////////////////////////////////////////////////////////////
//
// Point-Stat configuration file.
//
// For additional information, see the MET_BASE/config/README file.
//
////////////////////////////////////////////////////////////////////////////////

//
// Output model name to be written
//
model = "MODEL";

//
// Output description to be written
// May be set separately in each "obs.field" entry
//
desc = "NA";

////////////////////////////////////////////////////////////////////////////////

//
// Verification grid
//
regrid = {
   to_grid    = NONE;
   method     = NEAREST;
   width      = 1;
   vld_thresh = 0.5;
}

////////////////////////////////////////////////////////////////////////////////

cnt_thresh  = [ NA ];
cnt_logic   = UNION;
wind_thresh = [ NA ];
wind_logic  = UNION;

//
// Forecast and observation fields to be verified
//
fcst = {
   message_type = [ "ADPUPA" ];

   field = [
      {
        name       = "TMP";
        level      = [ "P925","P850","P700","P500","P300" ];
      },
      {
        name       = "RH";
        level      = [ "P925","P850","P700","P500","P300" ];
      },
      {
        name       = "HGT";
        level      = [ "P925","P850","P700","P500","P300" ];
      },
      {
        name       = "DPT";
        level      = [ "P925","P850","P700","P500","P300" ];
      },
      {
        name       = "UGRD";
        level      = [ "P925","P850","P700","P500","P300" ];
      },
      {
        name       = "VGRD";
        level      = [ "P925","P850","P700","P500","P300" ];
      },
      {
        name       = "WIND";
        level      = [ "P925","P850","P700","P500","P300" ];
      }
   ];

}
obs = fcst;

////////////////////////////////////////////////////////////////////////////////

//
// Point observation filtering options
// May be set separately in each "obs.field" entry
//
sid_exc        = [];
obs_quality_inc    = [];
duplicate_flag = NONE;
obs_summary    = NONE;
obs_percentile = 50;

////////////////////////////////////////////////////////////////////////////////

//
// Climatology mean data
//
climo_mean = {

   file_name = [];
   field     = [];

   regrid = {
      vld_thresh = 0.5;
      method     = NEAREST;
      width      = 1;
   }

   time_interp_method = DW_MEAN;
   match_day          = FALSE;
   time_step          = 21600;
}

////////////////////////////////////////////////////////////////////////////////

//
// Point observation time window
//
obs_window = {
   beg = -4000;
   end =  4000;
}

////////////////////////////////////////////////////////////////////////////////

//
// Verification masking regions
//
mask = {
   grid = [ "FULL" ];
   poly = [];
   sid  = [];
}

////////////////////////////////////////////////////////////////////////////////

//
// Confidence interval settings
//
ci_alpha  = [ 0.05 ];

boot = {
   interval = PCTILE;
   rep_prop = 1.0;
   n_rep    = 1000;
   rng      = "mt19937";
   seed     = "1";
}

////////////////////////////////////////////////////////////////////////////////

//
// Interpolation methods
//
interp = {
   vld_thresh = 1.0;

   type = [
      {
         method = NEAREST;
         width  = 1;
      }
   ];
}

////////////////////////////////////////////////////////////////////////////////

//
// HiRA verification method
//
hira = {
   flag       = FALSE;
   width      = [ 2, 3, 4, 5 ];
   vld_thresh = 1.0;
   cov_thresh = [ ==0.25 ];
}

////////////////////////////////////////////////////////////////////////////////

//
// Statistical output types
//
output_flag = {
   fho    = NONE;
   ctc    = NONE;
   cts    = NONE;
   mctc   = NONE;
   mcts   = NONE;
   cnt    = BOTH;
   sl1l2  = NONE;
   sal1l2 = NONE;
   vl1l2  = BOTH;
   val1l2 = BOTH;
   pct    = NONE;
   pstd   = NONE;
   pjc    = NONE;
   prc    = NONE;
   mpr    = BOTH;
}

////////////////////////////////////////////////////////////////////////////////

rank_corr_flag = TRUE;
tmp_dir        = "/tmp";
output_prefix  = "";
version        = "V10.0.0";

////////////////////////////////////////////////////////////////////////////////
